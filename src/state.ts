import { User } from 'firebase/auth';
import { reactive } from 'vue';

interface State {
  user: User;
}

const state = reactive<State>({
  user: null,
});

export default state;
