import axios from 'axios';

const SERVER_URL: string = 'https://c6b3d490325e.ngrok.io/piapp-b7b4d/us-central1';

export default class ServerManager {
    public requestApprovePayment = (paymentId: string) => {
      axios.post(`${SERVER_URL}/default-approve`, {
        paymentId,
      });
    }

    public requestCompletePayment = (paymentId: string, transactionId: string) => {
      axios.post(`${SERVER_URL}/default-complete`, {
        paymentId,
        transactionId,
      });
    }
}
