import { initializeApp } from 'firebase/app';
import FirestoreService from '@/services/firestore_service';
import AnalyticsService from '@/services/analytics_service';
import AuthService from '@/services/auth_service';
import { User } from 'firebase/auth';
import state from '@/state';

const FIREBASE_CONFIG = {
  apiKey: 'AIzaSyCEFDnmeG5Bkfqyk2qYp7SlJw3M1xVaZhI',
  authDomain: 'piapp-b7b4d.firebaseapp.com',
  projectId: 'piapp-b7b4d',
  storageBucket: 'piapp-b7b4d.appspot.com',
  messagingSenderId: '191247568349',
  appId: '1:191247568349:web:b4552dc1c5a44e586cba34',
  measurementId: 'G-4WMFZNFMLF',
};

initializeApp(FIREBASE_CONFIG);

export default class FirebaseManager {
  private firestoreService: FirestoreService;

  private analyticsService: AnalyticsService;

  private authService: AuthService;

  public constructor(authService: AuthService, firestoreService: FirestoreService, analyticsService: AnalyticsService) {
    this.authService = authService;
    this.firestoreService = firestoreService;
    this.analyticsService = analyticsService;

    this.authService.setAuthStateChangedCallback(this.onAuthStateChanged);
  }

  public signIn = () => this.authService.signIn();

  public signOut = () => this.authService.signOut();

  public addDocument = (path: string, data: Object) => this.firestoreService.addDocument(path, data);

  private onAuthStateChanged = (user: User) => {
    state.user = user;
  }
}
