import ServerManager from './server_manager';

declare const Pi: any;
const PI_SCOPES = ['payments'];
const { SANDBOX_MODE } = process.env;

export default class PiManager {
  private user: any | undefined;

  private inPiBrowser: boolean;

  private serverManager: ServerManager;

  constructor(serverManager: ServerManager) {
    this.serverManager = serverManager;
    this.user = undefined;
    this.inPiBrowser = false;

    // Initialize Pi
    Pi.init({ version: '2.0', sandbox: SANDBOX_MODE });
  }

  public init = async (): Promise<void> => {
    if (navigator.userAgent.includes('PiBrowser') || SANDBOX_MODE) {
      this.user = await Pi.authenticate(PI_SCOPES, this.onIncompletePaymentFound);
      this.inPiBrowser = true;
    } else {
      this.inPiBrowser = false;
    }
  }

  public getUser = (): any => this.user

  public createPayment = (amount: number, memo: string) => {
    if (!this.inPiBrowser) return;
    Pi.createPayment({ amount, memo, metadata: { ignore: true } }, {
      onReadyForServerApproval: this.serverManager.requestApprovePayment,
      onReadyForServerCompletion: this.serverManager.requestCompletePayment,
      onCancel: (paymentId: string) => { console.warn(`[Payment cancelled] ${paymentId}`); },
      onError: (error: Error) => { console.error(`[Payment error] ${error}`); },
    });
  }

  public isInPiBrowser = (): boolean => this.inPiBrowser;

  private onIncompletePaymentFound = (payment: any) => {
    console.error(`onIncompletePaymentFound: ${JSON.stringify(payment)}`);
  }
}
