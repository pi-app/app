import FirebaseManager from '@managers/firebase_manager';
import PiManager from '@managers/pi_manager';
import ServerManager from '@managers/server_manager';
import { createApp } from 'vue';
import ElementPlus from 'element-plus';
import App from './App.vue';
import AnalyticsService from './services/analytics_service';
import AuthService from './services/auth_service';
import FirestoreService from './services/firestore_service';
import 'element-plus/lib/theme-chalk/index.css';

const authService = new AuthService();
const firestoreService = new FirestoreService();
const analyticsService = new AnalyticsService();
const firebaseManager = new FirebaseManager(authService, firestoreService, analyticsService);
const serverManager = new ServerManager();
const piManager = new PiManager(serverManager);

piManager.init().then(() => {
  createApp(App)
    .use(ElementPlus)
    .provide('piManager', piManager)
    .provide('firebaseManager', firebaseManager)
    .mount('#app');
});
