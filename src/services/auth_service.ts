import {
  Auth, getAuth, GoogleAuthProvider, onAuthStateChanged, signInWithPopup, User,
} from 'firebase/auth';

export default class AuthService {
  private provider = new GoogleAuthProvider();

  private auth: Auth;

  public constructor() {
    this.auth = getAuth();
  }

  public setAuthStateChangedCallback = (callback: (user: User) => void) => onAuthStateChanged(this.auth, callback)

  public signIn = () => signInWithPopup(this.auth, this.provider);

  public signOut = () => this.auth.signOut();
}
