import {
  FirebaseFirestore, getFirestore, setDoc, doc,
} from 'firebase/firestore';

export default class FirestoreService {
  private firestore: FirebaseFirestore;

  constructor() {
    this.firestore = getFirestore();
  }

  public addDocument = async (collectionPath: string, data: Object) => {
    await setDoc(doc(this.firestore, collectionPath), data);
  }
}
