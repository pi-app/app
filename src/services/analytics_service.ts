import {
  getAnalytics,
  Analytics,
} from 'firebase/analytics';

export default class AnalyticsService {
  private analytics: Analytics

  constructor() {
    this.analytics = getAnalytics();
  }
}
