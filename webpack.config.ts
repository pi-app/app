/* eslint-disable import/no-extraneous-dependencies */
import { Configuration as WebpackConfiguration, DefinePlugin, EnvironmentPlugin } from 'webpack';
import { Configuration as WebpackDevServerConfiguration } from 'webpack-dev-server';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { VueLoaderPlugin } from 'vue-loader';

interface Configuration extends WebpackConfiguration {
  devServer?: WebpackDevServerConfiguration;
}

export default async (env: any, params: any): Promise<Configuration> => {
  const config: Configuration = {
    entry: `${__dirname}/src/main.ts`,
    devtool: 'inline-source-map',
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: [
            /node_modules/,
          ],
          use: {
            loader: 'ts-loader',
            options: {
              appendTsSuffixTo: [/\.vue$/],
            },
          },
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
        },
        {
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        },
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
      alias: {
        '@': `${__dirname}/src`,
        '@managers': `${__dirname}/src/managers`,
        '@components': `${__dirname}/src/components`,
        '@services': `${__dirname}/src/services`,
      },
    },
    output: {
      path: `${__dirname}/dist`,
    },
    plugins: [
      new EnvironmentPlugin({
        SANDBOX_MODE: params.mode === 'development',
      }),
      new DefinePlugin({
        __VUE_OPTIONS_API__: true,
        __VUE_PROD_DEVTOOLS__: false,
      }),
      new HtmlWebpackPlugin({
        title: 'Pi App',
        template: `${__dirname}/assets/html/index.html`,
        favicon: `${__dirname}/assets/icons/favicon.ico`,
      }),
      new VueLoaderPlugin(),
    ],
    devServer: {
      disableHostCheck: true,
    },
  };

  return config;
};
